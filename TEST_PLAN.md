## 1. Test Plan Introduction
This test plan describes test activities

## 2. Test Object
[Swagger Petstore](https://petstore.swagger.io/) is a sample server in order to develop API tests.
Tests are related to only "https://petstore.swagger.io/v2/pet" endpoit

## 3. Tested Features

| Test  | ID  | Description |
| ----- | --- |--------- |
| TestPets.addNewDog_checkPetNameInResponseBody | RQ1 | POST: pet/addPet Pet object that needs to be added to the store | 
| TestPets.findExistingDogWithId_CheckPetDetailsInResponseBody_AsExpected | RQ2 | GET: pet/getPetById Returns a single pet |
| TestPets.updateExistingDog_CheckPetDetailsInResponseBody_AsExpected | RQ3 | PUT: pet/updatePet Update and existing pet |
| TestPets.deleteADog_CheckPetNotExistedInList_AsExpected | RQ4 | DELETE: pet/deletePet Deletes a pet by id |


## 4. Test Type

It is a functional API testing which each endpoint is tested as separately

## 5. Test Objective

To verify the requirements which I specified in the "Tested Feautures" have been fulfilled.
Also to validate whether test object works as the user expects.

## 6. Test Approach

### Risk Based Testing Approach

* My point of view is that ensuring, even if the customer finds a bug in the production that does not stop him using the app.
* I conducted RBT approach to identify the vital functionality and key features since the limited resources and time concerns (Eg: The test should be delivered in a week). 
* Here is the RBT steps;
  1. First, risks should be identified
  2. Then I have made a risk assessment with RBT Matrix
  3. Finally, did risk mitigation

#### a. Risk Assessment Matrix Structure
Probability & Impact | Critical | Major | Modarete | Minor |
| --- | --- | --- | --- |--- |
| Very Likely| HIGH | HIGH | SERIOUS | MEDIUM |
|     Likely | HIGH | HIGH | SERIOUS | SERIOUS |
|    Possible| HIGH | SERIOUS | MEDIUM | LOW |
|   Unlikely | SERIOUS | MEDIUM | MEDIUM | LOW |

#### b. Feature Risks Traceability Matrix
 Requirement | Risk Level | Description |
| ----- | ---------- |--------- |
| RQ1 | HIGH | The user "VERY LIKELY" to add a pet to the store, the impact will be "CRITICAL"  | 
| RQ2 | HIGH | The user "VERY LIKELY" to want to see a single pet, the impact will be "CRITICAL" |
| RQ3 | HIGH | The user "VERY LIKELY" try to update an existing pet, its impact will be "CRITICAL" |
| RQ4 | HIGH | The user "VERY LIKELY" to delete a pet, its impact is "CRITICAL" |

#### c. Risk Mitigation
All tests handled by a functional test. 


# 7. Test Model
The functional tests in this project follow "Arrange-Act-Assert" model:

* Arrange, or set up: Pre-conditions for the test
* Act: Calling controller functions or methods
* Assert: Verify that conditions are true

# 8. What could be the next steps to your project?
1. Healthy check test
2. Headers and Cookies test
3. XML test
4. Performance test
