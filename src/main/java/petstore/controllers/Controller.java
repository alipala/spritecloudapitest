package petstore.controllers;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import petstore.dataentities.Pet;
import petstore.dataentities.Status;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

import java.util.List;

/***
 * 
 * It is the controller class includes the drive methods in it
 * @author		Ali Pala
 * 
 *
 */

public class Controller {
	
	public static String BASE_URL = "https://petstore.swagger.io/v2";
	public String PET_ENDPOINT = BASE_URL + "/pet";
	public String STORE_ENDPOINT = BASE_URL + "/store";
	private RequestSpecification requestSpecification;
	
	
	/**
	 * <p>This is constructor of Controller class</p>
	 * It sets BASE_URL, Content Type an Log level
	 */
	public Controller() {
		RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
		requestSpecBuilder.setBaseUri(BASE_URL);
		requestSpecBuilder.setContentType(ContentType.JSON);
		requestSpecBuilder.log(LogDetail.ALL);
		
		requestSpecification = requestSpecBuilder.build();
	}
	
	/**
	 * <p>This method adds a new pet to the Pet Store</p>
	 *  @param	pet object that includes pet informations
	 *  @return	pet in the body which is returned.
	 */
	public Pet addANewPet(Pet pet) {
		return 
			given(requestSpecification)
				.body(pet)
				.post(PET_ENDPOINT).as(Pet.class);
	}
	
	/**
	 * <p>This method finds a pet from the Pet Store by its status</p>
	 *  @param	status to describe the pet available, pending or sold
	 *  @return	findPetsByStatus list of pet which we look for
	 */
	public List<Pet> findPetsByStatus(Status status) {
		return 
			given(requestSpecification)
				.queryParam("status", Status.AVAILABLE)
				.get(PET_ENDPOINT + "/findByStatus")
			.then().log().all()
				.extract().body()
				.jsonPath().getList("", Pet.class);
				
	}
	
	/**
	 * <p>This method deletes a pet from Pet Store based on its id</p>
	 *  @param	pet object that includes pet informations
	 */
	public void deleteAPet(Pet pet) {
		given(requestSpecification)
			.pathParam("petId", pet.getId())
			.delete(PET_ENDPOINT + "/{petId}");
			
	}
	
	/**
	 * <p>This method is a verification after deleting a pet from Pet Store</p>
	 *  @param	pet object that includes pet informations
	 */
	public void verifyDeletedPet(Pet pet) {
		given(requestSpecification)
			.pathParam("petId", pet.getId())
			.get(PET_ENDPOINT + "/{petId}")
		.then()
			.body(containsString("Pet not found"));
	}
	
	/**
	 * <p>This method finds a pet in Pet Store by its id</p>
	 *  @param	pet object that includes pet informations
	 *  @return	pet in the body which is returned.
	 */
	public Pet findPetById(Pet pet) {
		return 
			given(requestSpecification)
				.pathParam("petId", pet.getId())
                .get(PET_ENDPOINT + "/{petId}").as(Pet.class);

	}
	
	/**
	 * <p>This method update the existing pet in Pet Store</p>
	 *  @param	pet object that includes pet informations
	 *  @return	pet in the body which is returned.
	 */
	public Pet updateExistingPet(Pet pet) {
		return 
			given(requestSpecification)
				.body(pet)
				.put(PET_ENDPOINT).as(Pet.class);
	}

}
