package petstore.dataentities;

/***
 * This is enum in order to define the status of the pet in Pet Store
 * @author Ali Pala
 *
 */
public enum Status {
	AVAILABLE("available"), 
	PENDING("pending"), 
	SOLD("sold");
	

	private String value;

	/**
	 * Set the status of the pet
	 * @param value of the status
	 */
	private Status(String value) {
		this.value = value;
	}
	
    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
