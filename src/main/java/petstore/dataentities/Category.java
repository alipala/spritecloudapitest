package petstore.dataentities;

/**
 * This class consists of fields and methods to create a category
 * in order to be used to protect category data
 * 
 * @author		Ali Pala
 * @version		1.0
 *
 */
public class Category {
	private Integer id = null;
	private String name = null;
	
    public Category() {
    	super();
    }
	  
	/**
	 * It is Category constructor
	 * @param id of the category
	 * @param name of the category
	 */
	public Category(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * Get id of the category
	 * @return id of the category
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Set id of the category
	 * @param id of the category
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Get the name of the category
	 * @return name of the category
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name of the category
	 * @param name of the category
	 */
	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		return "Category {id=" + id + ", name=" + name + "}";
	}


	
	
	
	

}
