package petstore.dataentities;

/**
 * This class consists of fields and methods to create tags
 * in order to be used to protect tags data. Encaptulation
 * 
 * @author		Ali Pala
 * @version		1.0
 *
 */

public class Tags {
	
	private Integer id;
	private String name;
	
	/**
	 * <p>This is constructor of Tag class</p>
	 * It sets related fields
	 */
	/**
	 * @param id of the tag
	 * @param name of the tag
	 */
	public Tags(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Tags() {
		super();
	}
	
	
	/**
	 * Get id of the tag
	 * @return id of the tag
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Set id of the tag
	 * @param id of the tag
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * Get name of the tag
	 * @return name of the tag
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Set name of the tag
	 * @param name of the tag
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Tags [id=" + id + ", name=" + name + "]";
	}






	
	

}
