package petstore.dataentities;

import java.util.ArrayList;
import java.util.List;

/**
 * This class consists of fields and methods to create a pet
 * in order to be used to protect pet data
 * 
 * @author		Ali Pala
 * @version		1.0
 *
 */
public class Pet {
	private Integer id;
	private Category category;
	private String name;
	private List<String> photoUrls = new ArrayList<>();
	private List<Tags> tags = new ArrayList<>();
	private Status status;
	
    public Pet() {
    	super();
    }
    
	/**
	 * <p>This is constructor of Pet class</p>
	 * It sets related fields
	 */
	/**
	 * @param id of the pet
	 * @param category of the pet that belongs to
	 * @param name of the pet
	 * @param photoUrls photo link of the pet
	 * @param tags of the pet
	 * @param status of the pet whether available, pending or sold
	 */
	public Pet(Integer id, Category category, String name, List<String> photoUrls, List<Tags> tags, Status status) {
		super();
		this.id = id;
		this.category = category;
		this.name = name;
		this.photoUrls = photoUrls;
		this.tags = tags;
		this.status = status;
	}

	/**
	 * Get id of the pet
	 * @return id of the pet
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Set id of the pet
	 * @param id of the pet
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Get category of the pet
	 * @return category of the pet
	 */
	public Category getCategory() {
		return category;
	}

	/**
	 * Set category of the pet
	 * @param  category of the pet
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	/**
	 * Get name of the pet
	 * @return name of the pet
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set name of the pet
	 * @param name of the pet
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get getPhotoUrls photo URL of the pet photo
	 * @return photoUrls of the pet
	 */
	public List<String> getPhotoUrls() {
		return photoUrls;
	}

	/**
	 * Set URL photo of the pet
	 * @param photoUrls of the pet
	 */
	public void setPhotoUrls(List<String> photoUrls) {
		this.photoUrls = photoUrls;
	}

	/**
	 * Get tag of the pet
	 * @return tags of the pet
	 */
	public List<Tags> getTags() {
		return tags;
	}

	/**
	 * Set tags of the pet
	 * @param tags of the pet
	 */
	public void setTags(List<Tags> tags) {
		this.tags = tags;
	}

	/**
	 * Get status of the pet
	 * @return status of the pet
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * Set status of the pet
	 * @param status of the pet
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Pet {id=" + id + ", category=" + category + ", name=" + name + ", photoUrls=" + photoUrls + ", tags="
				+ tags + ", status=" + status + "}";
	}
	
	
	
	
}
