/*
 * This class is test class includes all the basic tests for CRUD operations.
 * "https://petstore.swagger.io/" This is a sample Petstore server. 
 * You can find out more about Swagger at http://swagger.io
 */

package petstore;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;

import java.util.Collections;
import java.util.Random;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import petstore.controllers.Controller;
import petstore.dataentities.Category;
import petstore.dataentities.Pet;
import petstore.dataentities.Status;
import petstore.dataentities.Tags;
import petstore.listeners.TestListener;
import petstore.utilities.LogUtil;

/**
 * This class consists of exclusively of tests methods that operate 
 * API calls. Before the test runs, BeforeClass annotation prepare the 
 * required controller methods.
 * 
 * @author		Ali Pala
 * @version		1.0
 *
 */
@Listeners({TestListener.class})
@Feature("Basis CRUD Operations Test of API")
public class TestPets {
	private static final String PHOTO_URL = 
			"https://img.webmd.com/dtmcms/live/webmd/consumer_assets/site_images/article_thumbnails/other/cat_sitting_on_cat_tower_other/1800x1200_cat_sitting_on_cat_tower_other.jpg";
	Controller controller;
	Random rand = new Random();

	
	Pet pet = new Pet(
					rand.nextInt(10000), 
					new Category(1, "dogs"), 
					"Pretty Dog", 
					Collections.singletonList(PHOTO_URL),
					Collections.singletonList(new Tags(1, "golden-retriever")),
					Status.AVAILABLE);
	
	
	@BeforeClass
	public void setUp() {
		controller = new Controller();
	}
	
	/**
	 * <p>This method tests POST request and check the response body as expected</p>
	 */
	@Test(priority = 0, description = "Add New Dog Test")
	@Story("Pet object that needs to be added to the store")
	@Severity(SeverityLevel.CRITICAL)
	public void addNewDog_checkPetNameInResponseBody() {
		LogUtil.info("Add new dog test started");
		Pet response = controller.addANewPet(pet);
		assertThat(response.getName(), is(samePropertyValuesAs(pet.getName())));
	}
	
	/**
	 * <p>This method tests GET request and check the response body as expected</p>
	 */
	@Test(priority = 1, description = "Find Existing Dog Test")
	@Story("Find existing pet and return a single pet")
	@Severity(SeverityLevel.CRITICAL)
    public void findExistingDogWithId_CheckPetDetailsInResponseBody_AsExpected() {
        Pet response = controller.findPetById(pet);
        assertThat(response.getId(), is(samePropertyValuesAs(pet.getId())));
    }
    
	/**
	 * <p>This method tests PUT request and check the response body as expected</p>
	 */
	@Test(priority = 2, description = "Update Existing Dog Test")
	@Story("Update and existing pet")
	@Severity(SeverityLevel.CRITICAL)
    public void updateExistingDog_CheckPetDetailsInResponseBody_AsExpected() {
        pet.setName("New name for my pet");
        Pet petResponse = controller.updateExistingPet(pet);
        assertThat(petResponse.getName(), is(samePropertyValuesAs(pet.getName())));
    }
	
	/**
	 * <p>This method tests DELETE request and check the response body as expected</p>
	 */
	@Test(priority = 3, description = "Delete Dog Test")
	@Story("Deletes a pet by id")
	@Severity(SeverityLevel.CRITICAL)
    public void deleteADog_CheckPetNotExistedInList_AsExpected() {
        controller.deleteAPet(pet);
        controller.verifyDeletedPet(pet);
    }

}
