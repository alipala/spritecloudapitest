## 1. Introduction
It is a demonstrate project for testing [Petstore Swagger API](https://petstore.swagger.io/)

## 2. Test Plan
IMPORTANT: All details about the object, approach, features to be tested and more in [Test Plan](TEST_PLAN.md).

## 3. How to run the tests locally

Download and Install Required Pieces of software 
   1. Download Eclipse IDE or IntelliJ or something else [Eclipse](https://www.eclipse.org/downloads/packages/release/2021-03/r) for Mac.
   2. Download and install [Java](https://www.java.com/en/download/help/mac_install.html)

2. Clone the repository from your favourite editor. I have used Eclipse Version: 2021-03 (4.19.0)

```
https://gitlab.com/alipala/spritecloudapitest.git
```

3. There is pom.xml file in the project root directory to install all requirements. 

```
Rigth click "pom.xml" > Select "Maven" > Hit "Update Project..."
```

4. Ensure all dependencies work properly.

```
mvn dependency:analyze"
```

#### Note: As soon as you import the project from "GitLab", the dependencies will be installed automatically.


4. Goto project root directory. Run the tests. Note: It clears any compiled files you have, making sure that you're really compiling each module from scratch.

```
mvn clean install 
```


6. Goto project directory and check whether allure works

```
allure --version                                               
```

7. To see the test results in HTML format, run the command in order to start server:

```
 allure serve 
 ```
 
 8. If you want to generate a report and open directly from ".html" file, execute command below.
 
 ```
 allure generate ./allure-results --clean -o allure-reports
 ```

## 4. How to run the test in a CI/CD pipeline(GitLab)
1. Follow this link below and run the pipeline directly

```
https://gitlab.com/alipala/spritecloudapitest/-/pipelines
```

2. After execution been done, 
Goto Report stage > Press "Browse" button in Job artifacts > Open "allure-reports" folder > hit "index.html"
You must see the HTML report in a new tab.

## 5. Test Environment

* Java
* Eclipse IDE
* TestNG test framework
* Rest Assured API test framework
* Allure Report (HTML report with screenshots and logs)
* Gitlab Integration


## 6. How Report Looks
See a sample report belongs to previous job: [Sample Report](https://alipala.gitlab.io/-/spritecloudapitest/-/jobs/1563854158/artifacts/allure-reports/index.html)